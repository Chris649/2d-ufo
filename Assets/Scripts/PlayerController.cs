﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody2D rb2d;
    private int count;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        count = 0;
        winText.text = "";
        setCountText();
    }
	void FixedUpdate()
    {
        if (!Input.touchSupported)
        {
            winText.text = "NOT TOUCH ACTIVATED";
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            Vector2 movement = new Vector2(moveHorizontal, moveVertical);
            rb2d.AddForce(movement * speed);
        } else if (Input.touchSupported)
        {
            winText.text = "TOUCH MODE";
            float touchX = (Input.GetTouch(0).position.x - (Screen.currentResolution.width / 2))/100;
            float touchY = (Input.GetTouch(0).position.y - (Screen.currentResolution.height / 2))/100;

            Vector2 movement = new Vector2(touchX, touchY);
            rb2d.AddForce(movement * speed);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            setCountText();
        }
    }

    void setCountText()
    {
        if (count >= 12)
            winText.text = "You win!!!";
        countText.text = "Count: " + count.ToString();
    }
}
